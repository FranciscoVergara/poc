const { post_graphql } = require('./POST_Graphql')
const apiUrl = require('./Analytical_Framework_Queries').apiUrl;
const queries = require('./Analytical_Framework_Queries').queries;

jest.setTimeout(30000)
test('Find Catalog', async () => {
    let query = queries.findProperty;
    let start = new Date();
    res = await post_graphql(apiUrl, query);
    let responseTime = new Date() - start;
    expect(responseTime).toBeLessThan(2000);
    expect(res.status).toBe(200);
  });

test('in operator', async () => {
    let query = queries.example;
    res = await post_graphql(apiUrl, query);
    a = res.data.data;
    console.log("characters" in a);
    console.log(a.hasOwnProperty("characters"));    
    
    expect(res.status).toBe(200);
  });