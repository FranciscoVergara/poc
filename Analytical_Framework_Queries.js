module.exports = {
    apiUrl: 'http://172.18.4.42:18082/graphql',
    queries: {
        example: `{
            characters{
              name
            }
          }`,
        findCatalog: `{
            designModel: findCatalog(class: DESIGN_MODEL) {
              id
              code
              children {
                id
                code
                name
                orderNumber
                parentPropertyId
                type
              }
            }
          }`,
          findProperty: `{
            findProperty(propertyId: 8, parentPropertyId: 4) {
              id
              code
              name
              type
              dataType
              parentPropertyId
              orderNumber
              ui {
                visible
                minimum
                maximum
                multiple
                defaultValue
                disabled
              }
              children {
                id
                code
                name
                type
                dataType
                parentPropertyId
                ui {
                  visible
                  minimum
                  maximum
                  multiple
                  defaultValue
                  disabled
                }
                rules {
                  propertyId
                  type
                  expression
                  group
                }
              }
            }
          }`
    }

}